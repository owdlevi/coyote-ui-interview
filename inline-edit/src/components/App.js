import React, { useState } from 'react';
import { checkData } from '../utils/api';
import {
  STATUS_LOADING,
  STATUS_SUCCESS,
  STATUS_ERROR,
  SUCCESS_MESSAGE,
  ERROR_MESSAGE,
} from '../utils/config';
import Input from './Input';
import Text from './Text';
import Loader from './Loader';
import Message from './Message';
import './App.css';

const App = () => {
  const [isEditing, setIsEditing] = useState();
  const [inputValue, setInputValue] = useState('Hello World');
  const [status, setStatus] = useState();
  const [message, setMessage] = useState();

  const handleEdit = () => {
    if (status !== STATUS_LOADING) {
      setIsEditing(!isEditing);
      setStatus();
      setMessage();
    }
  };

  const inputHandler = val => {
    setInputValue(val);
  };

  const handleSubmit = () => {
    setIsEditing(false);
    setStatus(STATUS_LOADING);

    checkData(inputValue)
      .then(data => {
        setStatus(STATUS_SUCCESS);
        setMessage(SUCCESS_MESSAGE);
      })
      .catch(error => {
        setStatus(STATUS_ERROR);
        setMessage(ERROR_MESSAGE);
      });
  };
  return (
    <div className="App">
      <h1>coyote-ui-test</h1>
      <div className="AppContainer">
        <div className="FormWrapper">
          <div className={isEditing ? `inputWrapper` : `staticText`}>
            {isEditing ? (
              <Input value={inputValue} inputHandler={inputHandler} handleSubmit={handleSubmit} />
            ) : (
              <Text value={inputValue} handleClick={handleEdit} />
            )}
          </div>
          <div className="FormStatus">{status && <Loader status={status} />}</div>
        </div>
        {(status === STATUS_SUCCESS || status === STATUS_ERROR) && (
          <Message message={message} status={status} />
        )}
      </div>
    </div>
  );
};

export default App;
