import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

jest.useFakeTimers();

describe('<App />', () => {
  test('renders App component', () => {
    render(<App />);
  });

  test('when click on text show <Input /> component', async () => {
    const handleEdit = jest.fn();

    render(<App />);
    await fireEvent.click(screen.getByTestId('app-text'));
    expect(screen.getByTestId('text-input')).toBeInTheDocument();
  });

  test('when press Enter editing <Input /> should change to text', async () => {
    const newValue = 'Thanks for all the fish';

    render(<App />);
    fireEvent.click(screen.getByTestId('app-text'));
    expect(screen.getByTestId('text-input')).toBeInTheDocument();
    await fireEvent.change(screen.getByTestId('text-input'), {
      target: { value: newValue },
    });
    await fireEvent.keyDown(screen.getByTestId('text-input'), { key: 'Enter', code: 'Enter' });

    expect(screen.getByTestId('app-text')).toBeInTheDocument();
    expect(screen.getByText(newValue)).toBeInTheDocument();
  });

  describe('test submit', () => {
    test('when press Enter editing <Input /> show loader component', async () => {
      const newValue = 'Thanks for all the fish';
      render(<App />);

      await fireEvent.click(screen.getByTestId('app-text'));
      await fireEvent.change(screen.getByTestId('text-input'), {
        target: { value: newValue },
      });
      await fireEvent.keyDown(screen.getByTestId('text-input'), { key: 'Enter', code: 'Enter' });
      await expect(screen.getByTestId('loader')).toBeInTheDocument();
    });
  });
});
