import React, { useEffect, useRef } from 'react';

const Input = ({ value, inputHandler, handleSubmit }) => {
  const inputRef = useRef();

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const updateValue = e => {
    inputHandler(e.target.value);
  };

  const handleKeyDown = e => {
    if (e.key === 'Enter') handleSubmit();
  };

  return (
    <>
      <input
        ref={inputRef}
        className="inputField"
        data-testid="text-input"
        type="text"
        defaultValue={value}
        onChange={e => updateValue(e)}
        onKeyDown={e => handleKeyDown(e)}
        onBlur={() => handleSubmit()}
      />
    </>
  );
};

export default Input;
