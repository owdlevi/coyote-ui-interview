import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Input from './Input';

const inputHandler = jest.fn();
const handleSubmit = jest.fn();

describe('<Input /> ', () => {
  test('should contain the input with value `Hello World`', () => {
    const helloWorld = `Hello World`;

    const { getByDisplayValue } = render(<Input value={helloWorld} />);
    getByDisplayValue(helloWorld);
  });

  test('inputHandler to be called onChange', async () => {
    const newValue = 'Thanks for all the fish';

    render(<Input value="" inputHandler={inputHandler} handleSubmit={handleSubmit} />);

    await fireEvent.change(screen.getByTestId('text-input'), {
      target: { value: newValue },
    });
    expect(inputHandler).toHaveBeenCalledTimes(1);
  });

  test('handleSubmit to be called if key is not enter', async () => {
    render(<Input value="" inputHandler={inputHandler} handleSubmit={handleSubmit} />);

    await fireEvent.keyDown(screen.getByTestId('text-input'), { key: 'A', code: 'KeyA' });
    expect(handleSubmit).toHaveBeenCalledTimes(0);
  });

  test('handleSubmit to be called on enter', async () => {
    render(<Input value="" inputHandler={inputHandler} handleSubmit={handleSubmit} />);

    await fireEvent.keyDown(screen.getByTestId('text-input'), { key: 'Enter', code: 'Enter' });
    expect(handleSubmit).toHaveBeenCalledTimes(1);
  });
});
