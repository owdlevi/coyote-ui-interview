import React from 'react';
import { STATUS_LOADING, STATUS_SUCCESS } from '../../utils/config';

const Loader = ({ status }) => {
  return status === STATUS_LOADING ? (
    <div data-testid="loader" className="loader"></div>
  ) : (
    <div
      data-testid="status-icon"
      className={status === STATUS_SUCCESS ? `success` : `error`}></div>
  );
};

export default Loader;
