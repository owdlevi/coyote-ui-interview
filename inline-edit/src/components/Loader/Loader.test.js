import React from 'react';
import { render, screen } from '@testing-library/react';
import Loader from './Loader';
import { STATUS_LOADING, STATUS_SUCCESS } from '../../utils/config';

describe(' <Loader />', () => {
  test('renders without any prop', () => {
    render(<Loader />);
    expect(screen.getByTestId('status-icon')).toBeInTheDocument();
  });

  test('renders with loader element', () => {
    render(<Loader status={STATUS_LOADING} />);
    expect(screen.getByTestId('loader')).toBeInTheDocument();
    expect(screen.getByTestId('loader').classList.contains('loader')).toBe(true);
  });

  test('when status is STATUS_SUCCESS renders status-icon with `success` class', () => {
    render(<Loader status={STATUS_SUCCESS} />);
    expect(screen.getByTestId('status-icon').classList.contains('success')).toBe(true);
  });
  test('when status is empty renders status-icon with `error` class', () => {
    render(<Loader />);
    expect(screen.getByTestId('status-icon').classList.contains('error')).toBe(true);
  });
});
