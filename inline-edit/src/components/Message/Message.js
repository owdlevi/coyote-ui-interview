import React from 'react';
import { STATUS_SUCCESS } from '../../utils/config';

const Message = ({ message, status }) => {
  return (
    <div
      data-testid="message"
      className={status === STATUS_SUCCESS ? `message successText` : `message errorText`}>
      {message}
    </div>
  );
};

export default Message;
