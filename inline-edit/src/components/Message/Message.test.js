import React from 'react';
import { render, screen } from '@testing-library/react';
import Message from './Message';
import { STATUS_SUCCESS } from '../../utils/config';

describe(' <Message />', () => {
  test('renders with message', () => {
    const message = 'This is a message';

    render(<Message message={message} />);
    expect(screen.getByTestId('message')).toBeInTheDocument();
    expect(screen.getByText(message)).toBeInTheDocument();
  });

  test('when status STATUS_SUCCESS renders with successText class', () => {
    const message = 'This is a message';

    render(<Message message={message} status={STATUS_SUCCESS} />);
    expect(screen.getByTestId('message').classList.contains('successText')).toBe(true);
  });
});
