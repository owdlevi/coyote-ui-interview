import React from 'react';

const Text = ({ value, handleClick }) => {
  return (
    <div data-testid="app-text" onClick={() => handleClick()}>
      {value}
    </div>
  );
};

export default Text;
