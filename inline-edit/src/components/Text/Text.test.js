import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Text from './Text';

describe('Testing <Text /> component', () => {
  test('should contain the text `Hello World`', () => {
    const helloWorld = `Hello World`;

    render(<Text value={helloWorld} />);
    expect(screen.getByText(helloWorld)).toBeInTheDocument();
  });

  test('Click', () => {
    const helloWorld = `Hello World`;
    const handleEdit = jest.fn();

    render(<Text value={helloWorld} handleClick={handleEdit} />);

    fireEvent.click(screen.getByTestId('app-text'));
    expect(handleEdit).toHaveBeenCalledTimes(1);
  });
});
