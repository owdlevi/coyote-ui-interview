export const checkData = data => {
  let myFirstPromise = new Promise((resolve, reject) => {
    const keyword = 'success';
    const fish = 'Thanks for all the fish';

    setTimeout(function() {
      if (data.toLowerCase().includes(keyword) || data === fish) {
        resolve('success');
      }
      reject('error');
    }, 2000);
  });

  return myFirstPromise;
};
