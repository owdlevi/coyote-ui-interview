import { checkData } from './api';

describe('checkData', () => {
  test('reject for `Hello World`', () => {
    const data = 'Hello World';
    return expect(checkData(data)).rejects.toEqual('error');
  });

  test('resolve with success for `Thanks for all the fish`', () => {
    const data = 'Thanks for all the fish';
    return expect(checkData(data)).resolves.toEqual('success');
  });

  test('resolve with success for any text containing `success`', () => {
    const data = 'Hello world success';
    return expect(checkData(data)).resolves.toEqual('success');
  });
});
