// Loader component status
export const STATUS_LOADING = 'STATUS_LOADING';
export const STATUS_SUCCESS = 'STATUS_SUCCESS';
export const STATUS_ERROR = 'STATUS_ERROR';

// Success,Error messages
export const SUCCESS_MESSAGE = 'Your request was successful';
export const ERROR_MESSAGE = 'There was an error with your request';
